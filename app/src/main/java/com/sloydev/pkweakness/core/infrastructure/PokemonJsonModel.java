package com.sloydev.pkweakness.core.infrastructure;


public class PokemonJsonModel {
    public int number;
    public String name;
    public int color;
    public String[] weaknesses;
}
